﻿Imports System.Data
Imports MySql.Data.MySqlClient

Public Class Conexion
    Private strConexion_ As String
    Private conexion_ As MySqlConnection
    Private Property strConexion()
        Get
            Return strConexion_
        End Get
        Set(ByVal value)
            strConexion_ = value
        End Set
    End Property

    Public Property conexion() As MySqlConnection
        Get
            Return conexion_
        End Get
        Set(ByVal value As MySqlConnection)
            conexion_ = value
        End Set
    End Property
    Public Sub Abrir()
        Try
            strConexion_ = "server=localhost;Uid=root;Pwd='';Database=cine_eter"
            conexion_ = New MySqlConnection(strConexion)
            conexion_.Open()
        Catch ex As InvalidOperationException
            MsgBox("La base de datos ya esta abierta")
        Catch ex As MySqlException
            MsgBox("Hubo un problema al conectar con la base de datos")
        End Try
    End Sub
    Public Sub Cerrar()
        conexion.Close()
    End Sub
End Class