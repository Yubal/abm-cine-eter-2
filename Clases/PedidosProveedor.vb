﻿Imports System.Data
Imports MySql.Data.MySqlClient
Public Class PedidosProveedor
    Inherits Conexion
    Private Id_ As Integer
    Private CantidadProd_ As Integer
    Private IdProd_ As String
    Private IdProv_ As Integer
    Private IdGerente_ As Integer
    Private FechaPedido_ As Date

    Public Property Id() As Integer
        Get
            Return Id_
        End Get
        Set(ByVal value As Integer)
            Id_ = value
        End Set
    End Property

    Public Property CantidadProd() As String
        Get
            Return CantidadProd_
        End Get
        Set(ByVal value As String)
            CantidadProd_ = value
        End Set
    End Property

    Public Property IdProd() As String
        Get
            Return IdProd_
        End Get
        Set(ByVal value As String)
            IdProd_ = value
        End Set
    End Property

    Public Property IdProv() As Integer
        Get
            Return IdProv_
        End Get
        Set(ByVal value As Integer)
            IdProv_ = value
        End Set
    End Property
    Public Property IdGerente() As Integer
        Get
            Return IdGerente_
        End Get
        Set(ByVal value As Integer)
            IdGerente_ = value
        End Set
    End Property
    Public Property FechaPedido() As Date
        Get
            Return FechaPedido_
        End Get
        Set(ByVal value As Date)
            FechaPedido_ = value
        End Set
    End Property

    Public Sub Mostrar(ByVal grilla As DataGridView)
        Abrir()
        Dim strComando As String = "select  *  from pedidosproveedor "
        Dim mysqlComando As New MySqlCommand(strComando, conexion)
        Dim tabla As New DataTable
        tabla.Load(mysqlComando.ExecuteReader)
        grilla.DataSource = tabla
    End Sub
    

    Public Sub Agregar(ByVal pedidoP As PedidosProveedor)
        Abrir()
        Dim strComando As String = "INSERT INTO pedidosproveedor (Cantidad,IdProd,IdProv,IdGerente,FechaPedido) VALUES " & _
        "(@Cantidad,@IdProd,@IdProv,@IdGerente,@FechaPedido);"
        Dim mysqlComando As New MySqlCommand(strComando, conexion)
        mysqlComando.Parameters.AddWithValue("@Cantidad", pedidoP.CantidadProd)
        mysqlComando.Parameters.AddWithValue("@IdProd", pedidoP.IdProd)
        mysqlComando.Parameters.AddWithValue("@IdProv", pedidoP.IdProv)
        mysqlComando.Parameters.AddWithValue("@IdGerente", pedidoP.IdGerente)
        mysqlComando.Parameters.AddWithValue("@FechaPedido", pedidoP.FechaPedido)
        mysqlComando.ExecuteNonQuery()
        Cerrar()
    End Sub
    Public Sub Borrar(ByVal IdPedidoProv As Integer)
        Abrir()
        Dim strComando As String = "DELETE FROM pedidosproveedor WHERE id=@id"
        Dim mysqlComando As New MySqlCommand(strComando, conexion)
        mysqlComando.Parameters.AddWithValue("@id", IdPedidoProv)
        mysqlComando.ExecuteNonQuery()
        Cerrar()
    End Sub
    Public Sub Modificar(ByVal pedidoP As PedidosProveedor)
        Abrir()
        Dim strComando As String = "UPDATE pedidosproveedor SET Cantidad=@Cantidad,IdProd=@IdProd,IdProv=@IdProv,IdGerente=@IdGerente,FechaPedido=@FechaPedido WHERE id=@id"
        Dim mysqlComando As New MySqlCommand(strComando, conexion)
        mysqlComando.Parameters.AddWithValue("@Id", pedidoP.Id)
        mysqlComando.Parameters.AddWithValue("@Cantidad", pedidoP.CantidadProd)
        mysqlComando.Parameters.AddWithValue("@IdProd", pedidoP.IdProd)
        mysqlComando.Parameters.AddWithValue("@IdProv", pedidoP.IdProv)
        mysqlComando.Parameters.AddWithValue("@IdGerente", pedidoP.IdGerente)
        mysqlComando.Parameters.AddWithValue("@FechaPedido", pedidoP.FechaPedido)
        mysqlComando.ExecuteNonQuery()
        Cerrar()
    End Sub

    Public Sub CargarComboProductos(ByVal ComboActual As ComboBox, ByVal IdProveedores As Integer)
        Try
            Abrir()
            Dim strComando As String = "SELECT Id, Nombre FROM proveedor ORDER BY Nombre"
            Dim mysqlComando As New MySqlCommand(strComando, conexion)
            Dim objDataTable As New Data.DataTable
            objDataTable.Load(mysqlComando.ExecuteReader)
            'Dim objDataAdapter As New MySqlDataAdapter(mysqlComando)
            'objDataAdapter.Fill(objDataTable)
            With ComboActual
                .DataSource = objDataTable
                .DisplayMember = "Nombre"
                .ValueMember = "Id"
            End With
        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            Cerrar()
        End Try

    End Sub
    Public Sub MostrarProd(ByVal grilla As DataGridView, ByVal IdProd As Integer)
        Abrir()
        Dim strComando As String = "select pp.Id, pp.Prov, pp,Prod, p.Nombre, p.Descripcion, p.Cantidad, p.Precio FROM pedidosproveedor pp " & _
        "inner join producto p on pp.IdProd  = p.Id where IdProv = @IdProv"
        Dim MySqlComando As New MySqlCommand(strComando, conexion)
        MySqlComando.Parameters.AddWithValue("@IdProv", IdProv)
        Dim tabla As New DataTable
        tabla.Load(MySqlComando.ExecuteReader)
        grilla.DataSource = tabla
        grilla.Columns("Id").Width = 50
        grilla.Columns("Nombre").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        grilla.Columns("Descripcion").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        grilla.Columns("Cantidad").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        grilla.Columns("Precio").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill

    End Sub
    Public Sub MostrarProd2(ByVal grilla As DataGridView)
        Abrir()
        Dim strComando As String = "SELECT Id, Nombre FROM producto"
        Dim MySqlComando As New MySqlCommand(strComando, conexion)
        Dim tabla As New DataTable

        Try
            tabla.Load(MySqlComando.ExecuteReader)
            grilla.DataSource = tabla
            Cerrar()
        Catch ex As Exception
            MsgBox("pene" + ex.Message)
        End Try
    End Sub
End Class

