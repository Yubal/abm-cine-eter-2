﻿Imports System.Data
Imports MySql.Data.MySqlClient
Public Class Proveedores
    Inherits Conexion
    Private Id_ As Integer
    Private Nombre_ As String
    Private Telefono_ As String
    Private RFC_ As Integer


    Public Property Id() As Integer
        Get
            Return Id_
        End Get
        Set(ByVal value As Integer)
            Id_ = value
        End Set
    End Property

    Public Property Nombre() As String
        Get
            Return Nombre_
        End Get
        Set(ByVal value As String)
            Nombre_ = value
        End Set
    End Property

    Public Property Telefono() As Long
        Get
            Return Telefono_
        End Get
        Set(ByVal value As Long)
            Telefono_ = value
        End Set
    End Property

    Public Property RFC() As Integer
        Get
            Return RFC_
        End Get
        Set(ByVal value As Integer)
            RFC_ = value
        End Set
    End Property

    Public Sub Mostrar(ByVal grilla As DataGridView)
        Abrir()
        Dim strComando As String = "select  *  from proveedor"
        Dim mysqlComando As New MySqlCommand(strComando, conexion)
        Dim tabla As New DataTable
        tabla.Load(mysqlComando.ExecuteReader)
        grilla.DataSource = tabla
    End Sub
    'Public Sub CargarComboProductos(ByVal comboActual As ComboBox, ByVal idProducto As Integer)
    '   Try
    '      Abrir()
    'Dim strComando As String = "SELECT Id FROM producto ORDER BY Id"
    'Dim mysqlComando As New MySqlCommand(strComando, conexion)
    'Dim tabla As New Data.DataTable
    ' tabla.Load(mysqlComando.ExecuteReader)
    'With comboActual
    '             .DataSource = tabla
    '           .DisplayMember = "id"
    '            .ValueMember = "id"
    '        End With
    '    Catch ex As Exception
    '        MsgBox(ex.Message)
    '    Finally
    '        Cerrar()
    '    End Try
    'End Sub
    Public Sub Agregar(ByVal proveedor As Proveedores)
        Abrir()
        Dim strComando As String = "INSERT INTO proveedor (Nombre, Telefono, RFC) VALUES " & _
        "(@Nombre,@Telefono,@RFC);"
        Dim mysqlComando As New MySqlCommand(strComando, conexion)
        mysqlComando.Parameters.AddWithValue("@Nombre", proveedor.Nombre)
        mysqlComando.Parameters.AddWithValue("@Telefono", proveedor.Telefono)
        mysqlComando.Parameters.AddWithValue("@RFC", proveedor.RFC)
        mysqlComando.ExecuteNonQuery()
        Cerrar()
    End Sub
    Public Sub Modificar(ByVal proveedor As Proveedores)
        Abrir()
        Dim strComando As String = "UPDATE empleado SET Nombre=@Nombre,Telefono=@Telefono,RFC=@RFC,IdProd=@IdProd WHERE id=@id"
        Dim mysqlComando As New MySqlCommand(strComando, conexion)
        mysqlComando.Parameters.AddWithValue("@Id", proveedor.Id)
        mysqlComando.Parameters.AddWithValue("@Nombre", proveedor.Nombre)
        mysqlComando.Parameters.AddWithValue("@Telefono", proveedor.Telefono)
        mysqlComando.Parameters.AddWithValue("@RFC", proveedor.RFC)
        mysqlComando.ExecuteNonQuery()
        Cerrar()


    End Sub
    Public Sub Borrar(ByVal IdProveedor As Integer)
        Abrir()
        Dim strComando As String = "DELETE FROM proveedor WHERE id=@id"
        Dim mysqlComando As New MySqlCommand(strComando, conexion)
        mysqlComando.Parameters.AddWithValue("@id", IdProveedor)
        mysqlComando.ExecuteNonQuery()
        Cerrar()
    End Sub
End Class
