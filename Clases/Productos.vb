﻿Imports System.Data
Imports MySql.Data.MySqlClient
Public Class Productos
    Inherits Conexion
    Private Id_ As Integer
    Private Nombre_ As String
    Private Descripcion_ As String
    Private Cantidad_ As Integer
    Private Precio_ As Integer

    Public Property Id() As Integer
        Get
            Return Id_
        End Get
        Set(ByVal value As Integer)
            Id_ = value
        End Set
    End Property

    Public Property Nombre() As String
        Get
            Return Nombre_
        End Get
        Set(ByVal value As String)
            Nombre_ = value
        End Set
    End Property

    Public Property Descripcion() As String
        Get
            Return Descripcion_
        End Get
        Set(ByVal value As String)
            Descripcion_ = value
        End Set
    End Property

    Public Property Cantidad() As Integer
        Get
            Return Cantidad_
        End Get
        Set(ByVal value As Integer)
            Cantidad_ = value
        End Set
    End Property
    Public Property Precio() As Integer
        Get
            Return Precio_
        End Get
        Set(ByVal value As Integer)
            Precio_ = value
        End Set
    End Property

    Public Sub Mostrar(ByVal grilla As DataGridView)
        Abrir()
        Dim strComando As String = "select  *  from producto"
        Dim mysqlComando As New MySqlCommand(strComando, conexion)
        Dim tabla As New DataTable
        tabla.Load(mysqlComando.ExecuteReader)
        grilla.DataSource = tabla
    End Sub
    

    Public Sub Agregar(ByVal producto As Productos)
        Abrir()
        Dim strComando As String = "INSERT INTO producto (Nombre, Descripcion, Cantidad, Precio ) VALUES " & _
        "(@Nombre,@Descripcion,@Cantidad,@Precio);"
        Dim mysqlComando As New MySqlCommand(strComando, conexion)
        mysqlComando.Parameters.AddWithValue("@Nombre", producto.Nombre)
        mysqlComando.Parameters.AddWithValue("@Descripcion", producto.Descripcion)
        mysqlComando.Parameters.AddWithValue("@Cantidad", producto.Cantidad)
        mysqlComando.Parameters.AddWithValue("@Precio", producto.Precio)
        mysqlComando.ExecuteNonQuery()
        Cerrar()
    End Sub
    Public Sub Modificar(ByVal producto As Productos)
        Abrir()
        Dim strComando As String = "UPDATE producto SET Nombre=@Nombre,Descripcion=@Descripcion,Cantidad=@Cantidad,Precio=@Precio WHERE id=@id"
        Dim mysqlComando As New MySqlCommand(strComando, conexion)
        mysqlComando.Parameters.AddWithValue("@Id", producto.Id)
        mysqlComando.Parameters.AddWithValue("@Nombre", producto.Nombre)
        mysqlComando.Parameters.AddWithValue("@Descripcion", producto.Descripcion)
        mysqlComando.Parameters.AddWithValue("@Cantidad", producto.Cantidad)
        mysqlComando.Parameters.AddWithValue("@Precio", producto.Precio)
        mysqlComando.ExecuteNonQuery()
        Cerrar()


    End Sub
    Public Sub Borrar(ByVal IdProducto As Integer)
        Abrir()
        Dim strComando As String = "DELETE FROM producto WHERE id=@id"
        Dim mysqlComando As New MySqlCommand(strComando, conexion)
        mysqlComando.Parameters.AddWithValue("@id", IdProducto)
        mysqlComando.ExecuteNonQuery()
        Cerrar()
    End Sub

End Class
