﻿Imports System.Data
Imports MySql.Data.MySqlClient
Public Class Empleados
    Inherits Conexion
    Private Id_ As Integer
    Private Nombre_ As String
    Private Apellido_ As String
    Private Telefono_ As String

    Public Property Id() As Integer
        Get
            Return Id_
        End Get
        Set(ByVal value As Integer)
            Id_ = value
        End Set
    End Property

    Public Property Nombre() As String
        Get
            Return Nombre_
        End Get
        Set(ByVal value As String)
            Nombre_ = value
        End Set
    End Property

    Public Property Apellido() As String
        Get
            Return Apellido_
        End Get
        Set(ByVal value As String)
            Apellido_ = value
        End Set
    End Property

    Public Property Telefono() As Long
        Get
            Return Telefono_
        End Get
        Set(ByVal value As Long)
            Telefono_ = value
        End Set
    End Property

    Public Sub Mostrar(ByVal grilla As DataGridView)
        Abrir()
        Dim strComando As String = "select  *  from empleado"
        Dim mysqlComando As New MySqlCommand(strComando, conexion)
        Dim tabla As New DataTable
        tabla.Load(mysqlComando.ExecuteReader)
        grilla.DataSource = tabla
    End Sub

    Public Sub Agregar(ByVal empleado As Empleados)
        Abrir()
        Dim strComando As String = "INSERT INTO empleado (Nombre, Apellido, Telefono) VALUES " & _
        "(@Nombre,@Apellido,@Telefono);"
        Dim mysqlComando As New MySqlCommand(strComando, conexion)
        mysqlComando.Parameters.AddWithValue("@Nombre", empleado.Nombre)
        mysqlComando.Parameters.AddWithValue("@Apellido", empleado.Apellido)
        mysqlComando.Parameters.AddWithValue("@Telefono", empleado.Telefono)
        mysqlComando.ExecuteNonQuery()
        Cerrar()
    End Sub
    Public Sub Modificar(ByVal empleado As Empleados)
        Abrir()
        Dim strComando As String = "UPDATE empleado SET Nombre=@Nombre,Apellido=@Apellido,Telefono=@Telefono WHERE id=@id"
        Dim mysqlComando As New MySqlCommand(strComando, conexion)
        mysqlComando.Parameters.AddWithValue("@Id", empleado.Id)
        mysqlComando.Parameters.AddWithValue("@Nombre", empleado.Nombre)
        mysqlComando.Parameters.AddWithValue("@Apellido", empleado.Apellido)
        mysqlComando.Parameters.AddWithValue("@Telefono", empleado.Telefono)
        mysqlComando.ExecuteNonQuery()
        Cerrar()


    End Sub
    Public Sub Borrar(ByVal IdEmpleado As Integer)
        Abrir()
        Dim strComando As String = "DELETE FROM empleado WHERE id=@id"
        Dim mysqlComando As New MySqlCommand(strComando, conexion)
        mysqlComando.Parameters.AddWithValue("@id", IdEmpleado)
        mysqlComando.ExecuteNonQuery()
        Cerrar()
    End Sub
End Class
