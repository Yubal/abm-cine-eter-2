﻿Imports System.Data
Imports MySql.Data.MySqlClient
Public Class Gerentes
    Inherits Conexion
    Private Id_ As Integer
    Private Nombre_ As String
    Private Apellido_ As String
    Private Direccion_ As String
    Private Telefono_ As String

    Public Property Id() As Integer
        Get
            Return Id_
        End Get
        Set(ByVal value As Integer)
            Id_ = value
        End Set
    End Property

    Public Property Nombre() As String
        Get
            Return Nombre_
        End Get
        Set(ByVal value As String)
            Nombre_ = value
        End Set
    End Property
    Public Property Apellido() As String
        Get
            Return Apellido_
        End Get
        Set(ByVal value As String)
            Apellido_ = value
        End Set
    End Property

    Public Property Direccion() As String
        Get
            Return Direccion_
        End Get
        Set(ByVal value As String)
            Direccion_ = value
        End Set
    End Property

    Public Property Telefono() As Long
        Get
            Return Telefono_
        End Get
        Set(ByVal value As Long)
            Telefono_ = value
        End Set
    End Property
    Public Sub Mostrar(ByVal grilla As DataGridView)
        Abrir()
        Dim strComando As String = "select  *  from gerente"
        Dim mysqlComando As New MySqlCommand(strComando, conexion)
        Dim tabla As New DataTable
        tabla.Load(mysqlComando.ExecuteReader)
        grilla.DataSource = tabla
    End Sub
    Public Sub Agregar(ByVal gerente As Gerentes)
        Abrir()
        Dim strComando As String = "INSERT INTO gerente (Nombre, Apellido, Direccion, Telefono) VALUES " & _
        "(@Nombre,@Apellido,@Direccion,@Telefono);"
        Dim mysqlComando As New MySqlCommand(strComando, conexion)
        mysqlComando.Parameters.AddWithValue("@Nombre", gerente.Nombre)
        mysqlComando.Parameters.AddWithValue("@Apellido", gerente.Apellido)
        mysqlComando.Parameters.AddWithValue("@Direccion", gerente.Direccion)
        mysqlComando.Parameters.AddWithValue("@Telefono", gerente.Telefono)
        mysqlComando.ExecuteNonQuery()
        Cerrar()
    End Sub
    Public Sub Modificar(ByVal gerente As Gerentes)
        Abrir()
        Dim strComando As String = "UPDATE gerente SET Nombre=@Nombre,Apellido=@Apellido,Direccion=@Direccion,Telefono=@Telefono WHERE id=@id"
        Dim mysqlComando As New MySqlCommand(strComando, conexion)
        mysqlComando.Parameters.AddWithValue("@Id", gerente.Id)
        mysqlComando.Parameters.AddWithValue("@Nombre", gerente.Nombre)
        mysqlComando.Parameters.AddWithValue("@Apellido", gerente.Apellido)
        mysqlComando.Parameters.AddWithValue("@Direccion", gerente.Direccion)
        mysqlComando.Parameters.AddWithValue("@Telefono", gerente.Telefono)
        mysqlComando.ExecuteNonQuery()
        Cerrar()

    End Sub
    Public Sub Borrar(ByVal IdGerente As Integer)
        Abrir()
        Dim strComando As String = "DELETE FROM gerente WHERE id=@id"
        Dim mysqlComando As New MySqlCommand(strComando, conexion)
        mysqlComando.Parameters.AddWithValue("@id", IdGerente)
        mysqlComando.ExecuteNonQuery()
        Cerrar()
    End Sub
End Class
