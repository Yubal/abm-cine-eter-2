﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class EmpleadosForm
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.IdEmpTB = New System.Windows.Forms.TextBox
        Me.NombreEmpTB = New System.Windows.Forms.TextBox
        Me.ApellidoEmpTB = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.AceptarBtn = New System.Windows.Forms.Button
        Me.CancelarBtn = New System.Windows.Forms.Button
        Me.TelefonoEmpTB = New System.Windows.Forms.MaskedTextBox
        Me.SuspendLayout()
        '
        'IdEmpTB
        '
        Me.IdEmpTB.Enabled = False
        Me.IdEmpTB.Location = New System.Drawing.Point(76, 36)
        Me.IdEmpTB.Name = "IdEmpTB"
        Me.IdEmpTB.ReadOnly = True
        Me.IdEmpTB.Size = New System.Drawing.Size(47, 20)
        Me.IdEmpTB.TabIndex = 0
        '
        'NombreEmpTB
        '
        Me.NombreEmpTB.Location = New System.Drawing.Point(76, 70)
        Me.NombreEmpTB.Name = "NombreEmpTB"
        Me.NombreEmpTB.Size = New System.Drawing.Size(120, 20)
        Me.NombreEmpTB.TabIndex = 1
        '
        'ApellidoEmpTB
        '
        Me.ApellidoEmpTB.Location = New System.Drawing.Point(76, 107)
        Me.ApellidoEmpTB.Name = "ApellidoEmpTB"
        Me.ApellidoEmpTB.Size = New System.Drawing.Size(120, 20)
        Me.ApellidoEmpTB.TabIndex = 2
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(22, 39)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(16, 13)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "Id"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(22, 73)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(44, 13)
        Me.Label2.TabIndex = 5
        Me.Label2.Text = "Nombre"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(22, 110)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(44, 13)
        Me.Label3.TabIndex = 6
        Me.Label3.Text = "Apellido"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(22, 146)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(49, 13)
        Me.Label4.TabIndex = 7
        Me.Label4.Text = "Telefono"
        '
        'AceptarBtn
        '
        Me.AceptarBtn.Location = New System.Drawing.Point(25, 197)
        Me.AceptarBtn.Name = "AceptarBtn"
        Me.AceptarBtn.Size = New System.Drawing.Size(58, 23)
        Me.AceptarBtn.TabIndex = 8
        Me.AceptarBtn.Text = "Aceptar"
        Me.AceptarBtn.UseVisualStyleBackColor = True
        '
        'CancelarBtn
        '
        Me.CancelarBtn.Location = New System.Drawing.Point(128, 197)
        Me.CancelarBtn.Name = "CancelarBtn"
        Me.CancelarBtn.Size = New System.Drawing.Size(68, 23)
        Me.CancelarBtn.TabIndex = 9
        Me.CancelarBtn.Text = "Cancelar"
        Me.CancelarBtn.UseVisualStyleBackColor = True
        '
        'TelefonoEmpTB
        '
        Me.TelefonoEmpTB.Location = New System.Drawing.Point(76, 146)
        Me.TelefonoEmpTB.Mask = "99999999999"
        Me.TelefonoEmpTB.Name = "TelefonoEmpTB"
        Me.TelefonoEmpTB.Size = New System.Drawing.Size(100, 20)
        Me.TelefonoEmpTB.TabIndex = 10
        '
        'EmpleadosForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(228, 235)
        Me.Controls.Add(Me.TelefonoEmpTB)
        Me.Controls.Add(Me.CancelarBtn)
        Me.Controls.Add(Me.AceptarBtn)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.ApellidoEmpTB)
        Me.Controls.Add(Me.NombreEmpTB)
        Me.Controls.Add(Me.IdEmpTB)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Name = "EmpleadosForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Agregar Empleado"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents IdEmpTB As System.Windows.Forms.TextBox
    Friend WithEvents NombreEmpTB As System.Windows.Forms.TextBox
    Friend WithEvents ApellidoEmpTB As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents AceptarBtn As System.Windows.Forms.Button
    Friend WithEvents CancelarBtn As System.Windows.Forms.Button
    Friend WithEvents TelefonoEmpTB As System.Windows.Forms.MaskedTextBox
End Class
