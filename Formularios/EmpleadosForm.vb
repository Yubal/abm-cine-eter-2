﻿Public Class EmpleadosForm
    Private modo_ As String
    Private selEmpleado_ As Empleados

    Public WriteOnly Property modo() As String
        Set(ByVal value As String)
            modo_ = value
        End Set
    End Property

    Public WriteOnly Property selEmpleado() As Empleados
        Set(ByVal value As Empleados)
            selEmpleado_ = value
        End Set
    End Property

    Dim empleado As New Empleados


    Private Sub AceptarBtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AceptarBtn.Click

        empleado.Nombre = NombreEmpTB.Text
        empleado.Apellido = ApellidoEmpTB.Text
        empleado.Telefono = TelefonoEmpTB.Text


        If modo_ = "Agregar" Then
            empleado.Agregar(empleado)
        Else
            empleado.Id = IdEmpTB.Text
            empleado.Modificar(empleado)
        End If

        empleado.Mostrar(ListaEmpleado.DGVEmpleados)
        Close()
    End Sub

    Private Sub EmpleadosForm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Text = modo_ + " Empleado"

        If modo_ = "Modificar" Then
            IdEmpTB.Text = selEmpleado_.Id
            NombreEmpTB.Text = selEmpleado_.Nombre
            ApellidoEmpTB.Text = selEmpleado_.Apellido
            TelefonoEmpTB.Text = selEmpleado_.Telefono


        End If

    End Sub

    Private Sub CancelarBtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CancelarBtn.Click
        Close()
    End Sub






End Class