﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ListaEmpleados
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.DGVEmpleados = New System.Windows.Forms.DataGridView
        Me.AgregarEmpBtn = New System.Windows.Forms.Button
        Me.EliminarEmpBtn = New System.Windows.Forms.Button
        Me.ModificarEmpBtn = New System.Windows.Forms.Button
        Me.SalirEmpBtn = New System.Windows.Forms.Button
        Me.Label1 = New System.Windows.Forms.Label
        CType(Me.DGVEmpleados, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DGVEmpleados
        '
        Me.DGVEmpleados.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGVEmpleados.Location = New System.Drawing.Point(7, 60)
        Me.DGVEmpleados.Name = "DGVEmpleados"
        Me.DGVEmpleados.Size = New System.Drawing.Size(380, 200)
        Me.DGVEmpleados.TabIndex = 0
        '
        'AgregarEmpBtn
        '
        Me.AgregarEmpBtn.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.AgregarEmpBtn.Location = New System.Drawing.Point(7, 267)
        Me.AgregarEmpBtn.Name = "AgregarEmpBtn"
        Me.AgregarEmpBtn.Size = New System.Drawing.Size(75, 23)
        Me.AgregarEmpBtn.TabIndex = 1
        Me.AgregarEmpBtn.Text = "Agregar"
        Me.AgregarEmpBtn.UseVisualStyleBackColor = True
        '
        'EliminarEmpBtn
        '
        Me.EliminarEmpBtn.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.EliminarEmpBtn.Location = New System.Drawing.Point(169, 266)
        Me.EliminarEmpBtn.Name = "EliminarEmpBtn"
        Me.EliminarEmpBtn.Size = New System.Drawing.Size(75, 23)
        Me.EliminarEmpBtn.TabIndex = 2
        Me.EliminarEmpBtn.Text = "Eliminar"
        Me.EliminarEmpBtn.UseVisualStyleBackColor = True
        '
        'ModificarEmpBtn
        '
        Me.ModificarEmpBtn.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.ModificarEmpBtn.Location = New System.Drawing.Point(88, 267)
        Me.ModificarEmpBtn.Name = "ModificarEmpBtn"
        Me.ModificarEmpBtn.Size = New System.Drawing.Size(75, 23)
        Me.ModificarEmpBtn.TabIndex = 3
        Me.ModificarEmpBtn.Text = "Modificar"
        Me.ModificarEmpBtn.UseVisualStyleBackColor = True
        '
        'SalirEmpBtn
        '
        Me.SalirEmpBtn.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.SalirEmpBtn.Location = New System.Drawing.Point(315, 267)
        Me.SalirEmpBtn.Name = "SalirEmpBtn"
        Me.SalirEmpBtn.Size = New System.Drawing.Size(75, 23)
        Me.SalirEmpBtn.TabIndex = 4
        Me.SalirEmpBtn.Text = "Salir"
        Me.SalirEmpBtn.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(59, 13)
        Me.Label1.TabIndex = 5
        Me.Label1.Text = "Empleados"
        '
        'ListaEmpleados
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(389, 294)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.SalirEmpBtn)
        Me.Controls.Add(Me.ModificarEmpBtn)
        Me.Controls.Add(Me.EliminarEmpBtn)
        Me.Controls.Add(Me.AgregarEmpBtn)
        Me.Controls.Add(Me.DGVEmpleados)
        Me.Name = "ListaEmpleados"
        Me.Text = "ListaEmpleados"
        CType(Me.DGVEmpleados, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents DGVEmpleados As System.Windows.Forms.DataGridView
    Friend WithEvents AgregarEmpBtn As System.Windows.Forms.Button
    Friend WithEvents EliminarEmpBtn As System.Windows.Forms.Button
    Friend WithEvents ModificarEmpBtn As System.Windows.Forms.Button
    Friend WithEvents SalirEmpBtn As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
End Class
