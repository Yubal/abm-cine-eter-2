﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class PedidosProvForm
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.CancelarPPBtn = New System.Windows.Forms.Button
        Me.AceptarPPBtn = New System.Windows.Forms.Button
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.IdPPTB = New System.Windows.Forms.TextBox
        Me.Label6 = New System.Windows.Forms.Label
        Me.FechaPedidosTB = New System.Windows.Forms.MaskedTextBox
        Me.CMBIdProv = New System.Windows.Forms.ComboBox
        Me.DGVProductos = New System.Windows.Forms.DataGridView
        CType(Me.DGVProductos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'CancelarPPBtn
        '
        Me.CancelarPPBtn.Location = New System.Drawing.Point(384, 246)
        Me.CancelarPPBtn.Name = "CancelarPPBtn"
        Me.CancelarPPBtn.Size = New System.Drawing.Size(58, 23)
        Me.CancelarPPBtn.TabIndex = 23
        Me.CancelarPPBtn.Text = "Cancelar"
        Me.CancelarPPBtn.UseVisualStyleBackColor = True
        '
        'AceptarPPBtn
        '
        Me.AceptarPPBtn.Location = New System.Drawing.Point(41, 246)
        Me.AceptarPPBtn.Name = "AceptarPPBtn"
        Me.AceptarPPBtn.Size = New System.Drawing.Size(58, 23)
        Me.AceptarPPBtn.TabIndex = 22
        Me.AceptarPPBtn.Text = "Aceptar"
        Me.AceptarPPBtn.UseVisualStyleBackColor = True
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(38, 55)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(56, 13)
        Me.Label4.TabIndex = 20
        Me.Label4.Text = "Proveedor"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(37, 98)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(50, 13)
        Me.Label3.TabIndex = 19
        Me.Label3.Text = "Producto"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(38, 18)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(16, 13)
        Me.Label1.TabIndex = 17
        Me.Label1.Text = "Id"
        '
        'IdPPTB
        '
        Me.IdPPTB.Enabled = False
        Me.IdPPTB.Location = New System.Drawing.Point(142, 15)
        Me.IdPPTB.Name = "IdPPTB"
        Me.IdPPTB.ReadOnly = True
        Me.IdPPTB.Size = New System.Drawing.Size(33, 20)
        Me.IdPPTB.TabIndex = 12
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(37, 193)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(37, 13)
        Me.Label6.TabIndex = 25
        Me.Label6.Text = "Fecha"
        '
        'FechaPedidosTB
        '
        Me.FechaPedidosTB.Location = New System.Drawing.Point(142, 190)
        Me.FechaPedidosTB.Mask = "00/00/0000"
        Me.FechaPedidosTB.Name = "FechaPedidosTB"
        Me.FechaPedidosTB.Size = New System.Drawing.Size(103, 20)
        Me.FechaPedidosTB.TabIndex = 26
        Me.FechaPedidosTB.ValidatingType = GetType(Date)
        '
        'CMBIdProv
        '
        Me.CMBIdProv.FormattingEnabled = True
        Me.CMBIdProv.Location = New System.Drawing.Point(142, 52)
        Me.CMBIdProv.Name = "CMBIdProv"
        Me.CMBIdProv.Size = New System.Drawing.Size(180, 21)
        Me.CMBIdProv.TabIndex = 27
        '
        'DGVProductos
        '
        Me.DGVProductos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGVProductos.Location = New System.Drawing.Point(142, 98)
        Me.DGVProductos.Name = "DGVProductos"
        Me.DGVProductos.Size = New System.Drawing.Size(300, 79)
        Me.DGVProductos.TabIndex = 28
        '
        'PedidosProvForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(454, 303)
        Me.Controls.Add(Me.DGVProductos)
        Me.Controls.Add(Me.CMBIdProv)
        Me.Controls.Add(Me.FechaPedidosTB)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.CancelarPPBtn)
        Me.Controls.Add(Me.AceptarPPBtn)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.IdPPTB)
        Me.Name = "PedidosProvForm"
        Me.Text = "PedidosProvForm"
        CType(Me.DGVProductos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents CancelarPPBtn As System.Windows.Forms.Button
    Friend WithEvents AceptarPPBtn As System.Windows.Forms.Button
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents IdPPTB As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents FechaPedidosTB As System.Windows.Forms.MaskedTextBox
    Friend WithEvents CMBIdProv As System.Windows.Forms.ComboBox
    Friend WithEvents DGVProductos As System.Windows.Forms.DataGridView
End Class
