﻿Imports System.Data
Imports MySql.Data.MySqlClient

Public Class PedidosProvForm

    Private modo_ As String
    Private selPedProv_ As PedidosProveedor
    Public Con As MySqlConnection = New MySqlConnection("server=localhost;Uid=root;Pwd='';Database=cine_eter")


    Public WriteOnly Property modo() As String
        Set(ByVal value As String)
            modo_ = value
        End Set
    End Property

    Public WriteOnly Property selPedProv() As PedidosProveedor
        Set(ByVal value As PedidosProveedor)
            selPedProv_ = value
        End Set
    End Property

    Dim pedidoP As New PedidosProveedor
    Dim pp As New PedidosProveedor


    Private Sub PedidosProvForm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Con.State = ConnectionState.Closed Then
            Con.Open()

        End If

        Dim strComando As String = "SELECT Id, Nombre FROM proveedor ORDER BY Nombre"
        Dim mysqlComando As New MySqlCommand(strComando, Con)
        Dim tabla As New Data.DataTable
        tabla.Load(mysqlComando.ExecuteReader)
        With CMBIdProv
            .DataSource = tabla
            .DisplayMember = "Nombre"
            .ValueMember = "Id"
        End With

        pp.MostrarProd2(DGVProductos)

        'pedidoP.CargarComboProductos(CMBIdProd, selPedProv_.IdProd)




        If modo_ = "Modificar" Then
            IdPPTB.Text = selPedProv_.Id
            'CantidadPPTB.Text = selPedProv_.CantidadProd
            'ProductoPPTB.Text = selPedProv_.IdProd
            'IdProdPPTB.Text = selPedProv_.IdProd
            CMBIdProv.SelectedValue = selPedProv_.IdProv
            'GerentePPTB.Text = selPedProv_.IdGerente
            FechaPedidosTB.Text = selPedProv_.FechaPedido

        End If
        Con.Close()
    End Sub



    Private Sub AceptarPPBtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AceptarPPBtn.Click
        'pedidoP.CantidadProd = CantidadPPTB.Text
        pedidoP.IdProv = CMBIdProv.SelectedValue
        'pedidoP.IdProd = IdProdPPTB.Text
        'pedidoP.IdGerente = GerentePPTB.Text
        pedidoP.FechaPedido = FechaPedidosTB.Text

        If modo_ = "Agregar" Then
            pedidoP.Agregar(pedidoP)
        Else
            pedidoP.Id = IdPPTB.Text
            pedidoP.Modificar(pedidoP)
        End If

        pedidoP.Mostrar(ListaPedidosProv.DGVPedidosP)
        Close()
    End Sub

    Private Sub CancelarPPBtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CancelarPPBtn.Click
        Close()
    End Sub
End Class