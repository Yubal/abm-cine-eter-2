﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ProveedorForm
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.CancelarProvBtn = New System.Windows.Forms.Button
        Me.AceptarProvBtn = New System.Windows.Forms.Button
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.RFCProvTB = New System.Windows.Forms.TextBox
        Me.TelefonoProvTB = New System.Windows.Forms.TextBox
        Me.NombreProvTB = New System.Windows.Forms.TextBox
        Me.IdProvTB = New System.Windows.Forms.TextBox
        Me.SuspendLayout()
        '
        'CancelarProvBtn
        '
        Me.CancelarProvBtn.Location = New System.Drawing.Point(184, 183)
        Me.CancelarProvBtn.Name = "CancelarProvBtn"
        Me.CancelarProvBtn.Size = New System.Drawing.Size(58, 23)
        Me.CancelarProvBtn.TabIndex = 35
        Me.CancelarProvBtn.Text = "Cancelar"
        Me.CancelarProvBtn.UseVisualStyleBackColor = True
        '
        'AceptarProvBtn
        '
        Me.AceptarProvBtn.Location = New System.Drawing.Point(41, 183)
        Me.AceptarProvBtn.Name = "AceptarProvBtn"
        Me.AceptarProvBtn.Size = New System.Drawing.Size(58, 23)
        Me.AceptarProvBtn.TabIndex = 34
        Me.AceptarProvBtn.Text = "Aceptar"
        Me.AceptarProvBtn.UseVisualStyleBackColor = True
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(38, 135)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(28, 13)
        Me.Label4.TabIndex = 32
        Me.Label4.Text = "RFC"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(38, 96)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(49, 13)
        Me.Label3.TabIndex = 31
        Me.Label3.Text = "Telefono"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(38, 58)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(44, 13)
        Me.Label2.TabIndex = 30
        Me.Label2.Text = "Nombre"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(38, 21)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(16, 13)
        Me.Label1.TabIndex = 29
        Me.Label1.Text = "Id"
        '
        'RFCProvTB
        '
        Me.RFCProvTB.Location = New System.Drawing.Point(142, 132)
        Me.RFCProvTB.Name = "RFCProvTB"
        Me.RFCProvTB.Size = New System.Drawing.Size(100, 20)
        Me.RFCProvTB.TabIndex = 27
        '
        'TelefonoProvTB
        '
        Me.TelefonoProvTB.Location = New System.Drawing.Point(142, 93)
        Me.TelefonoProvTB.Name = "TelefonoProvTB"
        Me.TelefonoProvTB.Size = New System.Drawing.Size(100, 20)
        Me.TelefonoProvTB.TabIndex = 26
        '
        'NombreProvTB
        '
        Me.NombreProvTB.Location = New System.Drawing.Point(142, 55)
        Me.NombreProvTB.Name = "NombreProvTB"
        Me.NombreProvTB.Size = New System.Drawing.Size(100, 20)
        Me.NombreProvTB.TabIndex = 25
        '
        'IdProvTB
        '
        Me.IdProvTB.Enabled = False
        Me.IdProvTB.Location = New System.Drawing.Point(142, 18)
        Me.IdProvTB.Name = "IdProvTB"
        Me.IdProvTB.ReadOnly = True
        Me.IdProvTB.Size = New System.Drawing.Size(33, 20)
        Me.IdProvTB.TabIndex = 24
        '
        'ProveedorForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(284, 223)
        Me.Controls.Add(Me.CancelarProvBtn)
        Me.Controls.Add(Me.AceptarProvBtn)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.RFCProvTB)
        Me.Controls.Add(Me.TelefonoProvTB)
        Me.Controls.Add(Me.NombreProvTB)
        Me.Controls.Add(Me.IdProvTB)
        Me.Name = "ProveedorForm"
        Me.Text = "ProveedorForm"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents CancelarProvBtn As System.Windows.Forms.Button
    Friend WithEvents AceptarProvBtn As System.Windows.Forms.Button
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents RFCProvTB As System.Windows.Forms.TextBox
    Friend WithEvents TelefonoProvTB As System.Windows.Forms.TextBox
    Friend WithEvents NombreProvTB As System.Windows.Forms.TextBox
    Friend WithEvents IdProvTB As System.Windows.Forms.TextBox
End Class
