﻿Public Class ProductoForm
    Private modo_ As String
    Private selProducto_ As Productos

    Public WriteOnly Property modo() As String
        Set(ByVal value As String)
            modo_ = value
        End Set
    End Property

    Public WriteOnly Property selProducto() As Productos
        Set(ByVal value As Productos)
            selProducto_ = value
        End Set
    End Property

    Dim producto As New Productos


    Private Sub AceptarProdBtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AceptarProdBtn.Click

        producto.Nombre = NombreProdTB.Text
        producto.Descripcion = DescripcionProdTB.Text
        producto.Cantidad = CantidadProdTB.Text
        producto.Precio = PrecioProdTB.Text

        If modo_ = "Agregar" Then
            producto.Agregar(producto)
        Else
            producto.Id = IdProdTB.Text
            producto.Modificar(producto)
        End If

        producto.Mostrar(ListaProducto.DGVProductos)
        Close()
    End Sub

    Private Sub ProductosForm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Text = modo_ + " Producto"

        If modo_ = "Modificar" Then
            IdProdTB.Text = selProducto_.Id
            NombreProdTB.Text = selProducto_.Nombre
            DescripcionProdTB.Text = selProducto_.Descripcion
            CantidadProdTB.Text = selProducto_.Cantidad
            PrecioProdTB.Text = selProducto_.Precio
        End If
    End Sub

    Private Sub CancelarProdBtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CancelarProdBtn.Click
        Close()
    End Sub






End Class