﻿Public Class GerenteForm
    Private modo_ As String
    Private selGerente_ As Gerentes

    Public WriteOnly Property modo() As String
        Set(ByVal value As String)
            modo_ = value
        End Set
    End Property

    Public WriteOnly Property selGerente() As Gerentes
        Set(ByVal value As Gerentes)
            selGerente_ = value
        End Set
    End Property

    Dim gerente As New Gerentes

    Private Sub GerenteForm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Text = modo_ + " Gerente"

        If modo_ = "Modificar" Then
            IdGenTB.Text = selGerente_.Id
            NombreGenTB.Text = selGerente_.Nombre
            ApellidoGenTB.Text = selGerente_.Apellido
            DireccionGenTB.Text = selGerente_.Direccion
            TelefonoGenTB.Text = selGerente_.Telefono

        End If

    End Sub

    

    Private Sub AceptarGenBtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AceptarGenBtn.Click
        gerente.Nombre = NombreGenTB.Text
        gerente.Apellido = ApellidoGenTB.Text
        gerente.Direccion = DireccionGenTB.Text
        gerente.Telefono = TelefonoGenTB.Text


        If modo_ = "Agregar" Then
            gerente.Agregar(gerente)
        Else
            gerente.Id = IdGenTB.Text
            gerente.Modificar(gerente)
        End If

        gerente.Mostrar(ListaGerente.DGVGerentes)
        Close()
    End Sub

    Private Sub CancelarGenBtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CancelarGenBtn.Click
        Close()
    End Sub
End Class