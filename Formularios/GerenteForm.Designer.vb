﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class GerenteForm
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.IdGenTB = New System.Windows.Forms.TextBox
        Me.NombreGenTB = New System.Windows.Forms.TextBox
        Me.ApellidoGenTB = New System.Windows.Forms.TextBox
        Me.DireccionGenTB = New System.Windows.Forms.TextBox
        Me.TelefonoGenTB = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.AceptarGenBtn = New System.Windows.Forms.Button
        Me.CancelarGenBtn = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'IdGenTB
        '
        Me.IdGenTB.Enabled = False
        Me.IdGenTB.Location = New System.Drawing.Point(132, 28)
        Me.IdGenTB.Name = "IdGenTB"
        Me.IdGenTB.ReadOnly = True
        Me.IdGenTB.Size = New System.Drawing.Size(33, 20)
        Me.IdGenTB.TabIndex = 0
        '
        'NombreGenTB
        '
        Me.NombreGenTB.Location = New System.Drawing.Point(132, 65)
        Me.NombreGenTB.Name = "NombreGenTB"
        Me.NombreGenTB.Size = New System.Drawing.Size(100, 20)
        Me.NombreGenTB.TabIndex = 1
        '
        'ApellidoGenTB
        '
        Me.ApellidoGenTB.Location = New System.Drawing.Point(132, 103)
        Me.ApellidoGenTB.Name = "ApellidoGenTB"
        Me.ApellidoGenTB.Size = New System.Drawing.Size(100, 20)
        Me.ApellidoGenTB.TabIndex = 2
        '
        'DireccionGenTB
        '
        Me.DireccionGenTB.Location = New System.Drawing.Point(132, 142)
        Me.DireccionGenTB.Name = "DireccionGenTB"
        Me.DireccionGenTB.Size = New System.Drawing.Size(100, 20)
        Me.DireccionGenTB.TabIndex = 3
        '
        'TelefonoGenTB
        '
        Me.TelefonoGenTB.Location = New System.Drawing.Point(132, 179)
        Me.TelefonoGenTB.Name = "TelefonoGenTB"
        Me.TelefonoGenTB.Size = New System.Drawing.Size(100, 20)
        Me.TelefonoGenTB.TabIndex = 4
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(28, 31)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(16, 13)
        Me.Label1.TabIndex = 5
        Me.Label1.Text = "Id"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(28, 68)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(44, 13)
        Me.Label2.TabIndex = 6
        Me.Label2.Text = "Nombre"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(28, 106)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(44, 13)
        Me.Label3.TabIndex = 7
        Me.Label3.Text = "Apellido"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(28, 145)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(52, 13)
        Me.Label4.TabIndex = 8
        Me.Label4.Text = "Direccion"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(28, 182)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(49, 13)
        Me.Label5.TabIndex = 9
        Me.Label5.Text = "Telefono"
        '
        'AceptarGenBtn
        '
        Me.AceptarGenBtn.Location = New System.Drawing.Point(31, 226)
        Me.AceptarGenBtn.Name = "AceptarGenBtn"
        Me.AceptarGenBtn.Size = New System.Drawing.Size(58, 23)
        Me.AceptarGenBtn.TabIndex = 10
        Me.AceptarGenBtn.Text = "Aceptar"
        Me.AceptarGenBtn.UseVisualStyleBackColor = True
        '
        'CancelarGenBtn
        '
        Me.CancelarGenBtn.Location = New System.Drawing.Point(174, 226)
        Me.CancelarGenBtn.Name = "CancelarGenBtn"
        Me.CancelarGenBtn.Size = New System.Drawing.Size(58, 23)
        Me.CancelarGenBtn.TabIndex = 11
        Me.CancelarGenBtn.Text = "Cancelar"
        Me.CancelarGenBtn.UseVisualStyleBackColor = True
        '
        'GerenteForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(284, 261)
        Me.Controls.Add(Me.CancelarGenBtn)
        Me.Controls.Add(Me.AceptarGenBtn)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.TelefonoGenTB)
        Me.Controls.Add(Me.DireccionGenTB)
        Me.Controls.Add(Me.ApellidoGenTB)
        Me.Controls.Add(Me.NombreGenTB)
        Me.Controls.Add(Me.IdGenTB)
        Me.Name = "GerenteForm"
        Me.Text = "GerenteForm"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents IdGenTB As System.Windows.Forms.TextBox
    Friend WithEvents NombreGenTB As System.Windows.Forms.TextBox
    Friend WithEvents ApellidoGenTB As System.Windows.Forms.TextBox
    Friend WithEvents DireccionGenTB As System.Windows.Forms.TextBox
    Friend WithEvents TelefonoGenTB As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents AceptarGenBtn As System.Windows.Forms.Button
    Friend WithEvents CancelarGenBtn As System.Windows.Forms.Button
End Class
