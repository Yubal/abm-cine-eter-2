﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ProductoForm
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.CancelarProdBtn = New System.Windows.Forms.Button
        Me.AceptarProdBtn = New System.Windows.Forms.Button
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.CantidadProdTB = New System.Windows.Forms.TextBox
        Me.DescripcionProdTB = New System.Windows.Forms.TextBox
        Me.NombreProdTB = New System.Windows.Forms.TextBox
        Me.IdProdTB = New System.Windows.Forms.TextBox
        Me.PrecioProdTB = New System.Windows.Forms.TextBox
        Me.SuspendLayout()
        '
        'CancelarProdBtn
        '
        Me.CancelarProdBtn.Location = New System.Drawing.Point(186, 218)
        Me.CancelarProdBtn.Name = "CancelarProdBtn"
        Me.CancelarProdBtn.Size = New System.Drawing.Size(58, 23)
        Me.CancelarProdBtn.TabIndex = 23
        Me.CancelarProdBtn.Text = "Cancelar"
        Me.CancelarProdBtn.UseVisualStyleBackColor = True
        '
        'AceptarProdBtn
        '
        Me.AceptarProdBtn.Location = New System.Drawing.Point(43, 218)
        Me.AceptarProdBtn.Name = "AceptarProdBtn"
        Me.AceptarProdBtn.Size = New System.Drawing.Size(58, 23)
        Me.AceptarProdBtn.TabIndex = 22
        Me.AceptarProdBtn.Text = "Aceptar"
        Me.AceptarProdBtn.UseVisualStyleBackColor = True
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(40, 174)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(76, 13)
        Me.Label5.TabIndex = 21
        Me.Label5.Text = "Precio Unitario"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(40, 137)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(49, 13)
        Me.Label4.TabIndex = 20
        Me.Label4.Text = "Cantidad"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(40, 98)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(63, 13)
        Me.Label3.TabIndex = 19
        Me.Label3.Text = "Descripcion"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(40, 60)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(44, 13)
        Me.Label2.TabIndex = 18
        Me.Label2.Text = "Nombre"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(40, 23)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(16, 13)
        Me.Label1.TabIndex = 17
        Me.Label1.Text = "Id"
        '
        'CantidadProdTB
        '
        Me.CantidadProdTB.Location = New System.Drawing.Point(144, 134)
        Me.CantidadProdTB.Name = "CantidadProdTB"
        Me.CantidadProdTB.Size = New System.Drawing.Size(100, 20)
        Me.CantidadProdTB.TabIndex = 15
        '
        'DescripcionProdTB
        '
        Me.DescripcionProdTB.Location = New System.Drawing.Point(144, 95)
        Me.DescripcionProdTB.Name = "DescripcionProdTB"
        Me.DescripcionProdTB.Size = New System.Drawing.Size(100, 20)
        Me.DescripcionProdTB.TabIndex = 14
        '
        'NombreProdTB
        '
        Me.NombreProdTB.Location = New System.Drawing.Point(144, 57)
        Me.NombreProdTB.Name = "NombreProdTB"
        Me.NombreProdTB.Size = New System.Drawing.Size(100, 20)
        Me.NombreProdTB.TabIndex = 13
        '
        'IdProdTB
        '
        Me.IdProdTB.Enabled = False
        Me.IdProdTB.Location = New System.Drawing.Point(144, 20)
        Me.IdProdTB.Name = "IdProdTB"
        Me.IdProdTB.ReadOnly = True
        Me.IdProdTB.Size = New System.Drawing.Size(33, 20)
        Me.IdProdTB.TabIndex = 12
        '
        'PrecioProdTB
        '
        Me.PrecioProdTB.Location = New System.Drawing.Point(144, 171)
        Me.PrecioProdTB.Name = "PrecioProdTB"
        Me.PrecioProdTB.Size = New System.Drawing.Size(100, 20)
        Me.PrecioProdTB.TabIndex = 16
        '
        'ProductoForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(284, 261)
        Me.Controls.Add(Me.CancelarProdBtn)
        Me.Controls.Add(Me.AceptarProdBtn)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.PrecioProdTB)
        Me.Controls.Add(Me.CantidadProdTB)
        Me.Controls.Add(Me.DescripcionProdTB)
        Me.Controls.Add(Me.NombreProdTB)
        Me.Controls.Add(Me.IdProdTB)
        Me.Name = "ProductoForm"
        Me.Text = "ClienteForm"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents CancelarProdBtn As System.Windows.Forms.Button
    Friend WithEvents AceptarProdBtn As System.Windows.Forms.Button
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents CantidadProdTB As System.Windows.Forms.TextBox
    Friend WithEvents DescripcionProdTB As System.Windows.Forms.TextBox
    Friend WithEvents NombreProdTB As System.Windows.Forms.TextBox
    Friend WithEvents IdProdTB As System.Windows.Forms.TextBox
    Friend WithEvents PrecioProdTB As System.Windows.Forms.TextBox
End Class
