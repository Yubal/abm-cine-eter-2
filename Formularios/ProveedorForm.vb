﻿Public Class ProveedorForm
    Private modo_ As String
    Private selProveedor_ As Proveedores

    Public WriteOnly Property modo() As String
        Set(ByVal value As String)
            modo_ = value
        End Set
    End Property

    Public WriteOnly Property selProveedor() As Proveedores
        Set(ByVal value As Proveedores)
            selProveedor_ = value
        End Set
    End Property

    Dim proveedor As New Proveedores



    Private Sub ProveedorForm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Text = modo_ + " Proveedor"

        If modo_ = "Modificar" Then
            IdProvTB.Text = selProveedor_.Id
            NombreProvTB.Text = selProveedor_.Nombre
            TelefonoProvTB.Text = selProveedor_.Telefono
            RFCProvTB.Text = selProveedor_.RFC

        End If

    End Sub



    Private Sub AceptarProvBtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AceptarProvBtn.Click
        proveedor.Nombre = NombreProvTB.Text
        proveedor.Telefono = TelefonoProvTB.Text
        proveedor.RFC = RFCProvTB.Text


        If modo_ = "Agregar" Then
            proveedor.Agregar(proveedor)
        Else
            proveedor.Id = IdProvTB.Text
            proveedor.Modificar(proveedor)
        End If

        proveedor.Mostrar(ListaProveedor.DGVProveedor)
        Close()
    End Sub

    Private Sub CancelarProvBtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CancelarProvBtn.Click
        Close()
    End Sub
End Class