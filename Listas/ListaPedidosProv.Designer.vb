﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ListaPedidosProv
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.SalirPProvBtn = New System.Windows.Forms.Button
        Me.ModificarPProvBtn = New System.Windows.Forms.Button
        Me.EliminarPProvBtn = New System.Windows.Forms.Button
        Me.AgregarPProvBtn = New System.Windows.Forms.Button
        Me.DGVPedidosP = New System.Windows.Forms.DataGridView
        CType(Me.DGVPedidosP, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SalirPProvBtn
        '
        Me.SalirPProvBtn.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.SalirPProvBtn.Location = New System.Drawing.Point(501, 224)
        Me.SalirPProvBtn.Name = "SalirPProvBtn"
        Me.SalirPProvBtn.Size = New System.Drawing.Size(119, 23)
        Me.SalirPProvBtn.TabIndex = 27
        Me.SalirPProvBtn.Text = "Salir"
        Me.SalirPProvBtn.UseVisualStyleBackColor = True
        '
        'ModificarPProvBtn
        '
        Me.ModificarPProvBtn.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.ModificarPProvBtn.Location = New System.Drawing.Point(91, 224)
        Me.ModificarPProvBtn.Name = "ModificarPProvBtn"
        Me.ModificarPProvBtn.Size = New System.Drawing.Size(73, 23)
        Me.ModificarPProvBtn.TabIndex = 26
        Me.ModificarPProvBtn.Text = "Modificar"
        Me.ModificarPProvBtn.UseVisualStyleBackColor = True
        '
        'EliminarPProvBtn
        '
        Me.EliminarPProvBtn.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.EliminarPProvBtn.Location = New System.Drawing.Point(170, 224)
        Me.EliminarPProvBtn.Name = "EliminarPProvBtn"
        Me.EliminarPProvBtn.Size = New System.Drawing.Size(73, 23)
        Me.EliminarPProvBtn.TabIndex = 25
        Me.EliminarPProvBtn.Text = "Eliminar"
        Me.EliminarPProvBtn.UseVisualStyleBackColor = True
        '
        'AgregarPProvBtn
        '
        Me.AgregarPProvBtn.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.AgregarPProvBtn.Location = New System.Drawing.Point(12, 224)
        Me.AgregarPProvBtn.Name = "AgregarPProvBtn"
        Me.AgregarPProvBtn.Size = New System.Drawing.Size(73, 23)
        Me.AgregarPProvBtn.TabIndex = 24
        Me.AgregarPProvBtn.Text = "Agregar"
        Me.AgregarPProvBtn.UseVisualStyleBackColor = True
        '
        'DGVPedidosP
        '
        Me.DGVPedidosP.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGVPedidosP.Location = New System.Drawing.Point(12, 12)
        Me.DGVPedidosP.Name = "DGVPedidosP"
        Me.DGVPedidosP.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DGVPedidosP.Size = New System.Drawing.Size(608, 206)
        Me.DGVPedidosP.TabIndex = 23
        '
        'ListaPedidosProv
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(632, 261)
        Me.Controls.Add(Me.SalirPProvBtn)
        Me.Controls.Add(Me.ModificarPProvBtn)
        Me.Controls.Add(Me.EliminarPProvBtn)
        Me.Controls.Add(Me.AgregarPProvBtn)
        Me.Controls.Add(Me.DGVPedidosP)
        Me.Name = "ListaPedidosProv"
        Me.Text = "ListaPedidosProv"
        CType(Me.DGVPedidosP, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents SalirPProvBtn As System.Windows.Forms.Button
    Friend WithEvents ModificarPProvBtn As System.Windows.Forms.Button
    Friend WithEvents EliminarPProvBtn As System.Windows.Forms.Button
    Friend WithEvents AgregarPProvBtn As System.Windows.Forms.Button
    Friend WithEvents DGVPedidosP As System.Windows.Forms.DataGridView
End Class
