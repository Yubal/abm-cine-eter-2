﻿Public Class ListaGerente
    Dim gerente As New Gerentes
    Private Sub SalirGerBtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SalirGerBtn.Click
        Close()
    End Sub

    Private Sub AgregarGerBtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AgregarGerBtn.Click
        Dim ListaGerente As New ListaGerente
        GerenteForm.modo = "Agregar"
        GerenteForm.ShowDialog()
    End Sub

    Private Sub ListaGerentes_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        gerente.Mostrar(DGVGerentes)
    End Sub

    Private Sub ModificarGerBtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ModificarGerBtn.Click
        gerente.Id = DGVGerentes.Item("Id", DGVGerentes.CurrentRow.Index).Value
        gerente.Nombre = DGVGerentes.Item("Nombre", DGVGerentes.CurrentRow.Index).Value
        gerente.Apellido = DGVGerentes.Item("Apellido", DGVGerentes.CurrentRow.Index).Value
        gerente.Direccion = DGVGerentes.Item("Direccion", DGVGerentes.CurrentRow.Index).Value
        gerente.Telefono = DGVGerentes.Item("Telefono", DGVGerentes.CurrentRow.Index).Value


        Dim ListaGerente As New ListaGerente
        GerenteForm.modo = "Modificar"
        GerenteForm.selGerente = gerente
        GerenteForm.ShowDialog()
    End Sub

    Private Sub EliminarGerBtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EliminarGerBtn.Click
        Dim respuesta As MessageBoxOptions = MessageBox.Show("¿Está seguro que quiere borrar este Gerente...?", "Advertencia", MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2)
        If respuesta = 1 Then
            gerente.Id = DGVGerentes.Item("Id", DGVGerentes.CurrentRow.Index).Value
            gerente.Borrar(gerente.Id)
            gerente.Mostrar(DGVGerentes)
        End If
    End Sub
End Class