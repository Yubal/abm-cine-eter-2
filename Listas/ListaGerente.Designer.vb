﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ListaGerente
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label
        Me.SalirGerBtn = New System.Windows.Forms.Button
        Me.ModificarGerBtn = New System.Windows.Forms.Button
        Me.EliminarGerBtn = New System.Windows.Forms.Button
        Me.AgregarGerBtn = New System.Windows.Forms.Button
        Me.DGVGerentes = New System.Windows.Forms.DataGridView
        CType(Me.DGVGerentes, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(50, 13)
        Me.Label1.TabIndex = 11
        Me.Label1.Text = "Gerentes"
        '
        'SalirGerBtn
        '
        Me.SalirGerBtn.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.SalirGerBtn.Location = New System.Drawing.Point(328, 267)
        Me.SalirGerBtn.Name = "SalirGerBtn"
        Me.SalirGerBtn.Size = New System.Drawing.Size(75, 23)
        Me.SalirGerBtn.TabIndex = 10
        Me.SalirGerBtn.Text = "Salir"
        Me.SalirGerBtn.UseVisualStyleBackColor = True
        '
        'ModificarGerBtn
        '
        Me.ModificarGerBtn.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.ModificarGerBtn.Location = New System.Drawing.Point(88, 267)
        Me.ModificarGerBtn.Name = "ModificarGerBtn"
        Me.ModificarGerBtn.Size = New System.Drawing.Size(75, 23)
        Me.ModificarGerBtn.TabIndex = 9
        Me.ModificarGerBtn.Text = "Modificar"
        Me.ModificarGerBtn.UseVisualStyleBackColor = True
        '
        'EliminarGerBtn
        '
        Me.EliminarGerBtn.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.EliminarGerBtn.Location = New System.Drawing.Point(169, 267)
        Me.EliminarGerBtn.Name = "EliminarGerBtn"
        Me.EliminarGerBtn.Size = New System.Drawing.Size(75, 23)
        Me.EliminarGerBtn.TabIndex = 8
        Me.EliminarGerBtn.Text = "Eliminar"
        Me.EliminarGerBtn.UseVisualStyleBackColor = True
        '
        'AgregarGerBtn
        '
        Me.AgregarGerBtn.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.AgregarGerBtn.Location = New System.Drawing.Point(7, 267)
        Me.AgregarGerBtn.Name = "AgregarGerBtn"
        Me.AgregarGerBtn.Size = New System.Drawing.Size(75, 23)
        Me.AgregarGerBtn.TabIndex = 7
        Me.AgregarGerBtn.Text = "Agregar"
        Me.AgregarGerBtn.UseVisualStyleBackColor = True
        '
        'DGVGerentes
        '
        Me.DGVGerentes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGVGerentes.Location = New System.Drawing.Point(7, 46)
        Me.DGVGerentes.Name = "DGVGerentes"
        Me.DGVGerentes.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DGVGerentes.Size = New System.Drawing.Size(396, 200)
        Me.DGVGerentes.TabIndex = 6
        '
        'ListaGerente
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(415, 302)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.SalirGerBtn)
        Me.Controls.Add(Me.ModificarGerBtn)
        Me.Controls.Add(Me.EliminarGerBtn)
        Me.Controls.Add(Me.AgregarGerBtn)
        Me.Controls.Add(Me.DGVGerentes)
        Me.Name = "ListaGerente"
        Me.Text = "Lista de Gerentes"
        CType(Me.DGVGerentes, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents SalirGerBtn As System.Windows.Forms.Button
    Friend WithEvents ModificarGerBtn As System.Windows.Forms.Button
    Friend WithEvents EliminarGerBtn As System.Windows.Forms.Button
    Friend WithEvents AgregarGerBtn As System.Windows.Forms.Button
    Friend WithEvents DGVGerentes As System.Windows.Forms.DataGridView
End Class
