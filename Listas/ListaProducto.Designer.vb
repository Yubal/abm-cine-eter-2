﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ListaProducto
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label
        Me.SalirProdBtn = New System.Windows.Forms.Button
        Me.ModificarProdBtn = New System.Windows.Forms.Button
        Me.EliminarProdBtn = New System.Windows.Forms.Button
        Me.AgregarProdBtn = New System.Windows.Forms.Button
        Me.DGVProductos = New System.Windows.Forms.DataGridView
        CType(Me.DGVProductos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(17, 8)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(55, 13)
        Me.Label1.TabIndex = 17
        Me.Label1.Text = "Productos"
        '
        'SalirProdBtn
        '
        Me.SalirProdBtn.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.SalirProdBtn.Location = New System.Drawing.Point(449, 251)
        Me.SalirProdBtn.Name = "SalirProdBtn"
        Me.SalirProdBtn.Size = New System.Drawing.Size(119, 23)
        Me.SalirProdBtn.TabIndex = 16
        Me.SalirProdBtn.Text = "Salir"
        Me.SalirProdBtn.UseVisualStyleBackColor = True
        '
        'ModificarProdBtn
        '
        Me.ModificarProdBtn.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.ModificarProdBtn.Location = New System.Drawing.Point(114, 251)
        Me.ModificarProdBtn.Name = "ModificarProdBtn"
        Me.ModificarProdBtn.Size = New System.Drawing.Size(73, 23)
        Me.ModificarProdBtn.TabIndex = 15
        Me.ModificarProdBtn.Text = "Modificar"
        Me.ModificarProdBtn.UseVisualStyleBackColor = True
        '
        'EliminarProdBtn
        '
        Me.EliminarProdBtn.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.EliminarProdBtn.Location = New System.Drawing.Point(214, 251)
        Me.EliminarProdBtn.Name = "EliminarProdBtn"
        Me.EliminarProdBtn.Size = New System.Drawing.Size(73, 23)
        Me.EliminarProdBtn.TabIndex = 14
        Me.EliminarProdBtn.Text = "Eliminar"
        Me.EliminarProdBtn.UseVisualStyleBackColor = True
        '
        'AgregarProdBtn
        '
        Me.AgregarProdBtn.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.AgregarProdBtn.Location = New System.Drawing.Point(12, 251)
        Me.AgregarProdBtn.Name = "AgregarProdBtn"
        Me.AgregarProdBtn.Size = New System.Drawing.Size(73, 23)
        Me.AgregarProdBtn.TabIndex = 13
        Me.AgregarProdBtn.Text = "Agregar"
        Me.AgregarProdBtn.UseVisualStyleBackColor = True
        '
        'DGVProductos
        '
        Me.DGVProductos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGVProductos.Location = New System.Drawing.Point(12, 45)
        Me.DGVProductos.Name = "DGVProductos"
        Me.DGVProductos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DGVProductos.Size = New System.Drawing.Size(556, 200)
        Me.DGVProductos.TabIndex = 12
        '
        'ListaProducto
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(580, 286)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.SalirProdBtn)
        Me.Controls.Add(Me.ModificarProdBtn)
        Me.Controls.Add(Me.EliminarProdBtn)
        Me.Controls.Add(Me.AgregarProdBtn)
        Me.Controls.Add(Me.DGVProductos)
        Me.Name = "ListaProducto"
        Me.Text = "Lista de Productos"
        CType(Me.DGVProductos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents SalirProdBtn As System.Windows.Forms.Button
    Friend WithEvents ModificarProdBtn As System.Windows.Forms.Button
    Friend WithEvents EliminarProdBtn As System.Windows.Forms.Button
    Friend WithEvents AgregarProdBtn As System.Windows.Forms.Button
    Friend WithEvents DGVProductos As System.Windows.Forms.DataGridView
End Class
