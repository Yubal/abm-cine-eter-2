﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ListaProveedor
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label
        Me.SalirProvBtn = New System.Windows.Forms.Button
        Me.ModificarProvBtn = New System.Windows.Forms.Button
        Me.EliminarProvBtn = New System.Windows.Forms.Button
        Me.AgregarProvBtn = New System.Windows.Forms.Button
        Me.DGVProveedor = New System.Windows.Forms.DataGridView
        CType(Me.DGVProveedor, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(17, 6)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(67, 13)
        Me.Label1.TabIndex = 23
        Me.Label1.Text = "Proveedores"
        '
        'SalirProvBtn
        '
        Me.SalirProvBtn.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.SalirProvBtn.Location = New System.Drawing.Point(449, 249)
        Me.SalirProvBtn.Name = "SalirProvBtn"
        Me.SalirProvBtn.Size = New System.Drawing.Size(119, 23)
        Me.SalirProvBtn.TabIndex = 22
        Me.SalirProvBtn.Text = "Salir"
        Me.SalirProvBtn.UseVisualStyleBackColor = True
        '
        'ModificarProvBtn
        '
        Me.ModificarProvBtn.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.ModificarProvBtn.Location = New System.Drawing.Point(114, 249)
        Me.ModificarProvBtn.Name = "ModificarProvBtn"
        Me.ModificarProvBtn.Size = New System.Drawing.Size(73, 23)
        Me.ModificarProvBtn.TabIndex = 21
        Me.ModificarProvBtn.Text = "Modificar"
        Me.ModificarProvBtn.UseVisualStyleBackColor = True
        '
        'EliminarProvBtn
        '
        Me.EliminarProvBtn.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.EliminarProvBtn.Location = New System.Drawing.Point(219, 249)
        Me.EliminarProvBtn.Name = "EliminarProvBtn"
        Me.EliminarProvBtn.Size = New System.Drawing.Size(73, 23)
        Me.EliminarProvBtn.TabIndex = 20
        Me.EliminarProvBtn.Text = "Eliminar"
        Me.EliminarProvBtn.UseVisualStyleBackColor = True
        '
        'AgregarProvBtn
        '
        Me.AgregarProvBtn.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.AgregarProvBtn.Location = New System.Drawing.Point(12, 249)
        Me.AgregarProvBtn.Name = "AgregarProvBtn"
        Me.AgregarProvBtn.Size = New System.Drawing.Size(73, 23)
        Me.AgregarProvBtn.TabIndex = 19
        Me.AgregarProvBtn.Text = "Agregar"
        Me.AgregarProvBtn.UseVisualStyleBackColor = True
        '
        'DGVProveedor
        '
        Me.DGVProveedor.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGVProveedor.Location = New System.Drawing.Point(12, 43)
        Me.DGVProveedor.Name = "DGVProveedor"
        Me.DGVProveedor.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DGVProveedor.Size = New System.Drawing.Size(556, 200)
        Me.DGVProveedor.TabIndex = 18
        '
        'ListaProveedor
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(590, 274)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.SalirProvBtn)
        Me.Controls.Add(Me.ModificarProvBtn)
        Me.Controls.Add(Me.EliminarProvBtn)
        Me.Controls.Add(Me.AgregarProvBtn)
        Me.Controls.Add(Me.DGVProveedor)
        Me.Name = "ListaProveedor"
        Me.Text = "ListaProveedor"
        CType(Me.DGVProveedor, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents SalirProvBtn As System.Windows.Forms.Button
    Friend WithEvents ModificarProvBtn As System.Windows.Forms.Button
    Friend WithEvents EliminarProvBtn As System.Windows.Forms.Button
    Friend WithEvents AgregarProvBtn As System.Windows.Forms.Button
    Friend WithEvents DGVProveedor As System.Windows.Forms.DataGridView
End Class
