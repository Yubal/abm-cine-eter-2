﻿Public Class ListaProveedor
    Dim proveedor As New Proveedores
    Private Sub SalirProvBtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SalirProvBtn.Click
        Close()
    End Sub

    Private Sub AgregarProvBtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AgregarProvBtn.Click
        Dim ListaProveedor As New ListaProveedor
        ProveedorForm.modo = "Agregar"
        ProveedorForm.ShowDialog()
    End Sub

    Private Sub ListaProducto_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        proveedor.Mostrar(DGVProveedor)
    End Sub

    Private Sub ModificarProvBtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ModificarProvBtn.Click
        proveedor.Id = DGVProveedor.Item("Id", DGVProveedor.CurrentRow.Index).Value
        proveedor.Nombre = DGVProveedor.Item("Nombre", DGVProveedor.CurrentRow.Index).Value
        proveedor.Telefono = DGVProveedor.Item("Telefono", DGVProveedor.CurrentRow.Index).Value
        proveedor.RFC = DGVProveedor.Item("RFC", DGVProveedor.CurrentRow.Index).Value



        Dim ListaProveedor As New ListaProveedor
        ProveedorForm.modo = "Modificar"
        ProveedorForm.selProveedor = proveedor
        ProveedorForm.ShowDialog()
    End Sub

    Private Sub EliminarProvBtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EliminarProvBtn.Click
        Dim respuesta As MessageBoxOptions = MessageBox.Show("¿Está seguro que quiere borrar este Proveedor...?", "Advertencia", MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2)
        If respuesta = 1 Then
            proveedor.Id = DGVProveedor.Item("Id", DGVProveedor.CurrentRow.Index).Value
            proveedor.Borrar(proveedor.Id)
            proveedor.Mostrar(DGVProveedor)
        End If
    End Sub
End Class