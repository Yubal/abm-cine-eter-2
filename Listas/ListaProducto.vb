﻿Public Class ListaProducto
    Dim producto As New Productos
    Private Sub SalirProdBtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SalirProdBtn.Click
        Close()
    End Sub

    Private Sub AgregarProdBtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AgregarProdBtn.Click
        Dim ListaProducto As New ListaProducto
        ProductoForm.modo = "Agregar"
        ProductoForm.ShowDialog()
    End Sub

    Private Sub ListaProducto_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        producto.Mostrar(DGVProductos)
    End Sub

    Private Sub ModificarProdBtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ModificarProdBtn.Click
        producto.Id = DGVProductos.Item("Id", DGVProductos.CurrentRow.Index).Value
        producto.Nombre = DGVProductos.Item("Nombre", DGVProductos.CurrentRow.Index).Value
        producto.Descripcion = DGVProductos.Item("Descripcion", DGVProductos.CurrentRow.Index).Value
        producto.Cantidad = DGVProductos.Item("Cantidad", DGVProductos.CurrentRow.Index).Value
        producto.Precio = DGVProductos.Item("Precio", DGVProductos.CurrentRow.Index).Value


        Dim ListaProducto As New ListaProducto
        ProductoForm.modo = "Modificar"
        ProductoForm.selProducto = producto
        ProductoForm.ShowDialog()
    End Sub

    Private Sub EliminarProdBtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EliminarProdBtn.Click
        Dim respuesta As MessageBoxOptions = MessageBox.Show("¿Está seguro que quiere borrar este Producto...?", "Advertencia", MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2)
        If respuesta = 1 Then
            producto.Id = DGVProductos.Item("Id", DGVProductos.CurrentRow.Index).Value
            producto.Borrar(producto.Id)
            producto.Mostrar(DGVProductos)
        End If
    End Sub
End Class