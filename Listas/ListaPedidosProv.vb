﻿Public Class ListaPedidosProv
    Dim pedidosP As New PedidosProveedor
    Private Sub SalirProvBtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SalirPProvBtn.Click
        Close()
    End Sub

    Private Sub AgregarPProvBtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AgregarPProvBtn.Click
        Dim ListaPP As New ListaPedidosProv
        PedidosProvForm.modo = "Agregar"
        PedidosProvForm.ShowDialog()
    End Sub

    Private Sub ListaProducto_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        pedidosP.Mostrar(DGVPedidosP)
    End Sub

    Private Sub ModificarpProvBtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ModificarPProvBtn.Click
        pedidosP.Id = DGVPedidosP.Item("Id", DGVPedidosP.CurrentRow.Index).Value
        pedidosP.CantidadProd = DGVPedidosP.Item("Cantidad", DGVPedidosP.CurrentRow.Index).Value
        pedidosP.IdProd = DGVPedidosP.Item("IdProd", DGVPedidosP.CurrentRow.Index).Value
        pedidosP.IdProv = DGVPedidosP.Item("IdProv", DGVPedidosP.CurrentRow.Index).Value
        pedidosP.IdGerente = DGVPedidosP.Item("IdGerente", DGVPedidosP.CurrentRow.Index).Value


        Dim ListaPedidos As New ListaPedidosProv
        PedidosProvForm.modo = "Modificar"
        PedidosProvForm.selPedProv = pedidosP
        PedidosProvForm.ShowDialog()
    End Sub

    Private Sub EliminarPProvBtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EliminarPProvBtn.Click
        Dim respuesta As MessageBoxOptions = MessageBox.Show("¿Está seguro que quiere borrar este pedido a proveedores...?", "Advertencia", MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2)
        If respuesta = 1 Then
            pedidosP.Id = DGVPedidosP.Item("Id", DGVPedidosP.CurrentRow.Index).Value
            pedidosP.Borrar(pedidosP.Id)
            pedidosP.Mostrar(DGVPedidosP)
        End If
    End Sub
End Class