﻿Public Class ListaEmpleado
    Dim empleado As New Empleados
    Private Sub SalirEmpBtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SalirEmpBtn.Click
        Close()
    End Sub

    Private Sub AgregarEmpBtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AgregarEmpBtn.Click
        Dim ListaEmpleado As New ListaEmpleado
        EmpleadosForm.modo = "Agregar"
        EmpleadosForm.ShowDialog()
    End Sub

    Private Sub ListaEmpleados_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        empleado.Mostrar(DGVEmpleados)
    End Sub

    Private Sub ModificarEmpBtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ModificarEmpBtn.Click
        empleado.Id = DGVEmpleados.Item("Id", DGVEmpleados.CurrentRow.Index).Value
        empleado.Nombre = DGVEmpleados.Item("Nombre", DGVEmpleados.CurrentRow.Index).Value
        empleado.Apellido = DGVEmpleados.Item("Apellido", DGVEmpleados.CurrentRow.Index).Value
        empleado.Telefono = DGVEmpleados.Item("Telefono", DGVEmpleados.CurrentRow.Index).Value


        Dim ListaEmpleado As New ListaEmpleado
        EmpleadosForm.modo = "Modificar"
        EmpleadosForm.selEmpleado = empleado
        EmpleadosForm.ShowDialog()
    End Sub

    Private Sub EliminarEmpBtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EliminarEmpBtn.Click
        Dim respuesta As MessageBoxOptions = MessageBox.Show("¿Está seguro que quiere borrar este empleado...?", "Advertencia", MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2)
        If respuesta = 1 Then
            empleado.Id = DGVEmpleados.Item("Id", DGVEmpleados.CurrentRow.Index).Value
            empleado.Borrar(empleado.Id)
            empleado.Mostrar(DGVEmpleados)
        End If
    End Sub
End Class